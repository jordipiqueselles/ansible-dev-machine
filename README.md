# Description

This repo can be used to configure a Mac or Ubuntu machine using Ansible. It will install basic packages and utilities that developers may use.

# Instructions

Install Ansible

```
sudo pip3 install ansible
```

Run the playbook locally if you want to configure the current machine

```
ansible-playbook -i localhost, --connection local -K developer-pc.yml
```

If you want to run the playbook to configure other external targets you need to execute the following actions on the target machine:

- Install openssh
- Install python

On the current machine (the one that will execute Ansible)

```
ssh-copy-id <user>@<external-target>
ansible-playbook -i <ip-target>, -K developer-pc.yml
```
